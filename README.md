# GeoLocEval

usage:

    GeoLocEval [-h] [-d OUTDIR] [-g GEOLOC] [-o] [-a {nominatim,google}] oracle systems [systems ...]

Geographic Location Evaluation Framework for Geocoding Systems.

positional arguments:

    oracle                Ground-truth filepath
  
    systems               File/List/Dir of systems\' input filepaths

optional arguments:

    -h, --help            show this help message and exit
  
    -d OUTDIR, --outdir OUTDIR
  
                        Output directory
    -g GEOLOC, --geoloc GEOLOC
  
                        Filepath for cached geographic location mappings
                        
    -o, --online          Use an online geocoding API; default=False (offline)
  
    -a {nominatim,google}, --api {nominatim,google}
  
                        Geocoding API; default=nominatim

# Example:

For testing, run the following command:

    python geoloceval.py test\oracle.json -d test -g test\google.coords_cache.txt test\system_*.json

Tested with Python 2.7