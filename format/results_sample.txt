01/23/2019 12:30:16 PM oracle.doc.json
01/23/2019 12:30:17 PM Offline mode
01/23/2019 12:30:17 PM Number of cached locations is 1591, unresolved is 153
01/23/2019 12:30:17 PM cogeo.1.doc.json
01/23/2019 12:30:17 PM Offline mode
01/23/2019 12:30:17 PM Number of cached locations is 1591, unresolved is 296
01/23/2019 12:30:18 PM FujiXerox.1.doc.json
01/23/2019 12:30:18 PM Offline mode
01/23/2019 12:30:18 PM Number of cached locations is 1591, unresolved is 47
01/23/2019 12:30:19 PM Updated the offline coordinates cache.
01/23/2019 12:30:23 PM Saved formatted files to test\geoloceval.output.json
01/23/2019 12:30:25 PM Continuous evaluation
01/23/2019 12:30:25 PM Paired-test statistical evaluation based on error distance
01/23/2019 12:30:25 PM                               T-test(s)      T-test(p)     T`-test(s)     T`-test(p)  T`-test(mean)
01/23/2019 12:30:25 PM cogeo-FujiXerox                 44.2924            0.0      3569327.0            0.0      1896.3446
01/23/2019 12:30:25 PM 
01/23/2019 12:30:25 PM Discrete Evaluation at the level of COUNTY
01/23/2019 12:30:25 PM Number of originally unresolved locations: 1495 (14.950%)
01/23/2019 12:30:25 PM Number of unresolved locations for cogeo: 10000 (100.000%)
01/23/2019 12:30:25 PM Number of unresolved locations for FujiXerox: 1516 (15.160%)
01/23/2019 12:30:25 PM 
01/23/2019 12:30:25 PM Statistical significance test suite results
01/23/2019 12:30:25 PM                             s-test(g)    s-test(l)  s-test(two)   Pp-test(g)   Pp-test(l) Pp-test(two)   Rp-test(g)   Rp-test(l) Rp-test(two)   Ap-test(g)   Ap-test(l) Ap-test(two)  F1S-test(g)  F1S-test(l)F1S-test(two)  F1T-test(s)  F1T-test(p) F1T`-test(s) F1T`-test(p)    F1T`-mean   PS-test(g)   PS-test(l) PS-test(two)   PT-test(s)   PT-test(p)  PT`-test(s)  PT`-test(p)     PT`-mean   RS-test(g)   RS-test(l) RS-test(two)   RT-test(s)   RT-test(p)  RT`-test(s)  RT`-test(p)     RT`-mean
01/23/2019 12:30:30 PM cogeo-FujiXerox                   1.0          0.0          0.0          1.0          0.0          0.0          1.0          0.0          0.0          1.0          0.0          0.0          1.0          0.0          0.0     -29.9907          0.0          0.0          0.0      -0.2856          1.0          0.0          0.0     -29.8902          0.0          0.0          0.0      -0.3561          1.0          0.0          0.0     -28.5295          0.0        109.0          0.0      -0.2819
01/23/2019 12:30:31 PM 
01/23/2019 12:30:31 PM 
01/23/2019 12:30:31 PM Discrete evaluation results
01/23/2019 12:30:31 PM                         acc    acc161    medErr   meanErr     mPrec      mRec       mF1     MPrec      MRec       MF1
01/23/2019 12:30:31 PM cogeo                0.1495    0.4379     630.0    2860.0       0.0       0.0       0.0       0.0       0.0       0.0
01/23/2019 12:30:31 PM FujiXerox            0.5101    0.6681      20.0     963.0    0.4714    0.4702    0.4708    0.3559    0.2824    0.2854
01/23/2019 12:30:31 PM 
01/23/2019 12:31:01 PM *******************************************************
01/23/2019 12:31:01 PM Kendall's Tau-b rank correlations
01/23/2019 12:31:01 PM All metrics combinations at the same granularity level
01/23/2019 12:31:01 PM 
01/23/2019 12:31:01 PM At the level of COUNTY
01/23/2019 12:31:01 PM labels:
[('acc', 'acc161'), ('acc', 'medErr'), ('acc', 'meanErr'), ('acc', 'mPrec'), ('acc', 'mRec'), ('acc', 'mF1'), ('acc', 'MPrec'), ('acc', 'MRec'), ('acc', 'MF1'), ('acc161', 'medErr'), ('acc161', 'meanErr'), ('acc161', 'mPrec'), ('acc161', 'mRec'), ('acc161', 'mF1'), ('acc161', 'MPrec'), ('acc161', 'MRec'), ('acc161', 'MF1'), ('medErr', 'meanErr'), ('medErr', 'mPrec'), ('medErr', 'mRec'), ('medErr', 'mF1'), ('medErr', 'MPrec'), ('medErr', 'MRec'), ('medErr', 'MF1'), ('meanErr', 'mPrec'), ('meanErr', 'mRec'), ('meanErr', 'mF1'), ('meanErr', 'MPrec'), ('meanErr', 'MRec'), ('meanErr', 'MF1'), ('mPrec', 'mRec'), ('mPrec', 'mF1'), ('mPrec', 'MPrec'), ('mPrec', 'MRec'), ('mPrec', 'MF1'), ('mRec', 'mF1'), ('mRec', 'MPrec'), ('mRec', 'MRec'), ('mRec', 'MF1'), ('mF1', 'MPrec'), ('mF1', 'MRec'), ('mF1', 'MF1'), ('MPrec', 'MRec'), ('MPrec', 'MF1'), ('MRec', 'MF1')]
01/23/2019 12:31:01 PM tau-b:
[[ 0.   0.   0.   0.   0.   0.   0.   0.   0.   0. ]
 [ 0.6  0.   0.   0.   0.   0.   0.   0.   0.   0. ]
 [-0.8 -0.8  0.   0.   0.   0.   0.   0.   0.   0. ]
 [-0.8 -0.8  1.   0.   0.   0.   0.   0.   0.   0. ]
 [ 0.4  0.  -0.2 -0.2  0.   0.   0.   0.   0.   0. ]
 [ 0.6  0.2 -0.4 -0.4  0.4  0.   0.   0.   0.   0. ]
 [ 0.6  0.2 -0.4 -0.4  0.4  1.   0.   0.   0.   0. ]
 [ 0.6  0.2 -0.4 -0.4  0.4  1.   1.   0.   0.   0. ]
 [ 0.6  0.2 -0.4 -0.4  0.4  1.   1.   1.   0.   0. ]
 [ 0.6  0.2 -0.4 -0.4  0.4  1.   1.   1.   1.   0. ]]
01/23/2019 12:31:01 PM p-value:
[[ 0.          0.          0.          0.          0.          0.          0.          0.          0.          0.        ]
 [ 0.1416447   0.          0.          0.          0.          0.          0.          0.          0.          0.        ]
 [ 0.05004353  0.05004353  0.          0.          0.          0.          0.          0.          0.          0.        ]
 [ 0.05004353  0.05004353  0.01430588  0.          0.          0.          0.          0.          0.          0.        ]
 [ 0.32718689  1.          0.62420612  0.62420612  0.          0.          0.          0.          0.          0.        ]
 [ 0.1416447   0.62420612  0.32718689  0.32718689  0.32718689  0.          0.          0.          0.          0.        ]
 [ 0.1416447   0.62420612  0.32718689  0.32718689  0.32718689  0.01430588  0.          0.          0.          0.        ]
 [ 0.1416447   0.62420612  0.32718689  0.32718689  0.32718689  0.01430588  0.01430588  0.          0.          0.        ]
 [ 0.1416447   0.62420612  0.32718689  0.32718689  0.32718689  0.01430588  0.01430588  0.01430588  0.          0.        ]
 [ 0.1416447   0.62420612  0.32718689  0.32718689  0.32718689  0.01430588  0.01430588  0.01430588  0.01430588  0.        ]]
01/23/2019 12:31:04 PM 
01/23/2019 12:31:05 PM 
01/23/2019 12:31:05 PM *******************************************************
01/23/2019 12:31:05 PM All geographic granularity combinations for the same metric
01/23/2019 12:31:05 PM 
01/23/2019 12:31:05 PM COUNTY-CITY
01/23/2019 12:31:05 PM labels:
['acc', 'acc161', 'medErr', 'meanErr', 'mPrec', 'mRec', 'mF1', 'MPrec', 'MRec', 'MF1']
01/23/2019 12:31:05 PM tau-b:
[[ 1.  1.  1.  1.  1.  1.  1.  1.  1.  1.]]
01/23/2019 12:31:05 PM p-value:
[[ 0.01430588  0.01430588  0.01430588  0.01430588  0.01430588  0.01430588  0.01430588  0.01430588  0.01430588  0.01430588]]
01/23/2019 12:31:05 PM 
01/23/2019 12:31:05 PM 
