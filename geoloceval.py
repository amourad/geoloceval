# _*_ coding:utf-8 _*_
#**********************************************************************************************************************
# author:       Ahmed Mourad
# date:         16 Jan. 2019
# description:  GeoLocEval
#**********************************************************************************************************************
import argparse
import os
import sys
import glob
import codecs
import unicodecsv
import logging
from itertools import izip, combinations
from ast import literal_eval
from collections import defaultdict, namedtuple, OrderedDict
import json

import numpy as np
np.set_printoptions(threshold=np.nan, linewidth=np.nan)
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()
import pandas as pd

from sklearn import metrics
from scipy import interp, stats
import statsmodels.api as sm
from scipy.stats import wilcoxon, ttest_rel

from geopy.distance import vincenty, great_circle

from multiprocessing import Pool, cpu_count
from functools import partial

ERROR_DISTANCE_TOLERANCE = 161.0
GOOGLE_API_KEY = ""                 # TODO: To be replaced by the user API credentials
    
# geocoding api's quota per day
NOMINATIM_LIMIT = 2500
GOOGLE_LIMIT = 100000

def geocode(coords, api):
	""" Call geocoding api """
	if api == 'nominatim':
		from geopy.geocoders import Nominatim
		geocoder = Nominatim(timeout=10)
	elif api == 'google':
		from geopy.geocoders import GoogleV3
		geocoder = GoogleV3(api_key=GOOGLE_API_KEY, timeout=10)
	
	# Try 2 times because some locations can't be resolved form the first time
	for t in range(2):
		try:
			location = geocoder.reverse(coords, exactly_one=True)
			location = location.raw
			break
		except:
			location = {u'exception':str(sys.exc_info()[1])}
			if 'Too many requests' in location:
				logging.info('Too many requests submitted to the geocoding API.')
				break
	return location

def resolve_to_location(coords, online, api, cached_coords=None):
    """ Resolve GPS coordinates to a location
    Supported APIs are Google V3 (commercial) and Nominatem (free)

    Params:
    -------
    coords          :   list
        List of pairs of coordinates [(lat, lon)]
    cached_coords   :   dict of dicts
        Cached goegraphical location mappings.
        Nominatim geospatial api asks clients to cache results, otherwise will be blocked.
    online          :   bool
        Use online APIs to resolve coordinates to locations
    api             :   str
        Geocoding api to use; nominatim is the default free api, google is commercial
    
    Returns:
    --------
    y_resolved  :   list of dicts
        Resolved labels in the format
        [{"lat":lat, "lon":lon, "country":country, "state":state, "city":city,
        "city_district":city_district, "town":town, "county":county},]
    """
    
    unresolved = set(coords)
    
    if cached_coords:
        # filter out previously resolved locations
        unresolved.difference_update(set(cached_coords.keys()))

    if unresolved and online:
        unresolved = list(unresolved)
        limit = NOMINATIM_LIMIT
        
        if api == 'google':
            limit = GOOGLE_LIMIT    #100000
        
        cores = cpu_count()
        pool = Pool(processes=cores)
        
        size = min(unresolved, limit)
        if size < cores:
            chunksize = size
        else:
            chunksize = size/cores
            logging.debug('Cores {}, Chunk size {}'.format(cores, chunksize))
        locations = list(pool.map(partial(geocode, api=api), unresolved[:size], chunksize=chunksize))
        
        # Close and wait for pool to finish
        pool.close()
        pool.join()
        
        # update the geolocations lexicon with resolved addresses
        cached_coords.update([(coord, loc) for coord, loc in izip(unresolved, locations) if 'address' in loc or 'address_components' in loc])
        logging.debug('Number of resolved locations {}/{}'.format(len([l for l in locations if 'address' in l or 'address_components' in l]), size))
        logging.debug('Number of returned locations {}'.format(len(locations)))
    else:
        logging.info('Offline mode')
        logging.info('Number of cached locations is {}, unresolved is {}'.format(len(cached_coords), len(unresolved)))
    
    y_resolved = []
    for lat, lon in coords:
        location = cached_coords.get((lat, lon), {})
        country = state = county = city = 'unresolved'
        
        if 'address' in location:
            # nominatim
            country = location['address'].get('country', 'unresolved')
            state = location['address'].get('state', 'unresolved')
            county = location['address'].get('county', 'unresolved')
            city = location['address'].get('city', 'unresolved')
        elif 'address_components' in location:
            # google
            for ac in location['address_components']:
                # iterate over different address components returned by Google api
                if 'country' in ac['types']:                            # country
                    country = ac.get('long_name', 'unresolved')
                elif 'administrative_area_level_1' in ac['types']:      # state
                    state = ac.get('long_name', 'unresolved')
                elif 'administrative_area_level_2' in ac['types']:      # county
                    county = ac.get('long_name', 'unresolved')
                elif 'locality' in ac['types']:                         # city
                    city = ac.get('long_name', 'unresolved')
            
        y_resolved.append({"lat":lat, "lon":lon,
                           "country":country,
                           "state":state,
                           "city":city,
                           "county":county})

    return y_resolved

def evaluate_continuous(error_dist, systems, outdir):
    """ Continuous evaluation based on the error distance
    
    It generates a boxplot and measures the statistical significance
    of the differences between systems using Wilcoxon & ttest_relevance
    
    Params:
    -------
    error_dist  :   list of lists
        List of error distances in km per system
    systems      :   list
        List of labels (systems) aligned with error_dist
    outdir      :   string
        Output directory to save the generated graph
    """
    # Mappings for plotting in a specific order with labels aligned with tables in the paper
    mappings = OrderedDict([('csiro_1', 'Csiro.1'), ('csiro_2', 'Csiro.2'), ('csiro_3', 'Csiro.3'),
                            ('FujiXerox_1', 'FujiXerox.1'), ('FujiXerox_2', 'FujiXerox.2'), ('FujiXerox_3', 'FujiXerox.3'),
                            ('Drexel_1', 'Drexel.1'), ('Drexel_2', 'Drexel.2'), ('Drexel_3', 'Drexel.3'),
                            ('cogeo_1', 'IBM.1'), ('aist_1', 'AIST.1')])
    x, labels = [], []
    for k, v in mappings.items():
        if k in systems:
            labels.append(v)
            x.append(error_dist[systems.index(k)])
    if not x:
        x = error_dist
        labels = systems

    error_dist_df = pd.DataFrame(dict(zip(labels, x)))
    
    # sort the dataframe by column's median in an ascending order for plotting
    error_dist_meds = error_dist_df.median()
    error_dist_meds.sort_values(inplace=True)
    plot_df = error_dist_df[error_dist_meds.index[:8]]
    
    outdir = os.path.join(outdir, 'continuous')
    if(not os.path.isdir(outdir)):
        os.mkdir(outdir)
    
    # Box-plot **********************
    plot_df.plot.box(showmeans=True, showfliers=False, ylim=(0, 6000))
    plt.xlabel('Geolocation systems')
    plt.ylabel('Error distance in km')
    plt.title('Error Distance Evaluation')
    plt.savefig(os.path.join(outdir, 'Error Distance.png'), bbox_inches='tight')
    plt.show()
    plt.clf()
    plt.close()
    
    # statistical significance *********
    logging.info('Continuous evaluation')
    logging.info('Paired-test statistical evaluation based on error distance')
    if labels == systems:
        tmpStr = str(' ').rjust(24)
    else:
        tmpStr = str(' ').rjust(13)
    
    for m in ['T-test(s)', 'T-test(p)', 'T`-test(s)', 'T`-test(p)', 'T`-test(mean)']:
        tmpStr += str(m).rjust(15)
    logging.info(tmpStr)
    
    for c1, c2 in combinations(range(len(labels)), 2):
        if labels == systems:
            tmpStr = '{}-{}'.format(labels[c1], labels[c2]).ljust(24)
        else:
            tmpStr = '{}-{}'.format(labels[c1], labels[c2]).ljust(13)
        
        s, p = ttest_rel(error_dist[c1], error_dist[c2])
        tmpStr += repr(round(s, 4)).rjust(15)
        tmpStr += repr(round(p, 4)).rjust(15)
        
        s, p = wilcoxon(error_dist[c1], error_dist[c2])
        tmpStr += repr(round(s, 4)).rjust(15)
        tmpStr += repr(round(p, 4)).rjust(15)
        Wmean = np.mean(np.array(error_dist[c1]) - np.array(error_dist[c2]))
        tmpStr += repr(round(Wmean, 4)).rjust(15)
        logging.info(tmpStr)
    logging.info('')
    
    return

def test_significance(target, data):
    """ Null hypothesis significance testing
    
    A frequentist approach based on the following paper:
    
    Yiming Yang. 1999. "An evaluation of statistical approaches to text categorization."
    Information retrieval1, 1-2 (1999), 69–90.

    Parameters:
    -----------
    target  :   list
        List of target labels
    data    :   defaultdict(list)
        Dictionary of systems predictions; <system, list<predictions>>
    """
    tmpStr = str(' ').rjust(24)
    
    for m in ['s-test(g)', 's-test(l)', 's-test(two)',
              'Pp-test(g)', 'Pp-test(l)', 'Pp-test(two)',
              'Rp-test(g)', 'Rp-test(l)', 'Rp-test(two)',
              'Ap-test(g)', 'Ap-test(l)', 'Ap-test(two)',
              'F1S-test(g)', 'F1S-test(l)', 'F1S-test(two)',
              'F1T-test(s)', 'F1T-test(p)',
              'F1T`-test(s)', 'F1T`-test(p)', 'F1T`-mean',
              'PS-test(g)', 'PS-test(l)', 'PS-test(two)',
              'PT-test(s)', 'PT-test(p)',
              'PT`-test(s)', 'PT`-test(p)', 'PT`-mean',
              'RS-test(g)', 'RS-test(l)', 'RS-test(two)',
              'RT-test(s)', 'RT-test(p)',
              'RT`-test(s)', 'RT`-test(p)', 'RT`-mean']:
        tmpStr += str(m).rjust(13)
    logging.info(tmpStr)

    for s1, s2 in combinations(data.keys(), 2):
        tmpStr = '{}-{}'.format(s1, s2).ljust(24)
        
        # M -> number of unique categories
        M = len(set(target))
        # List of unique categories
        labels = list(set(target))
        
        # ********* micro sign test (s-test, binomial test)
        # A -> Binary decisions for s1 per test user
        A = [1 if p == t else 0 for p, t in zip(data[s1], target)]
        # B -> Binary decisions for s2 per test user
        B = [1 if p == t else 0 for p, t in zip(data[s2], target)]
        
        # n -> the number of times that A and B differ
        n = sum(i != j for i, j in zip(A, B))
        # k -> the number of times that A is larger than B
        k = sum(1 for i, j in zip(A, B) if i > j)
        
        p_value = stats.binom_test(k, n, alternative='greater')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        p_value = stats.binom_test(k, n, alternative='less')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        p_value = stats.binom_test(k, n, alternative='two-sided')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        # ********* comparing proportions (p-test, proportions z-test)
        # Followed this blog post to calculate the Z-score and P-values for two population proportions test
        # http://knowledgetack.com/python/statsmodels/two-sample-hypothesis-testing-in-python-with-statsmodels/
        #
        # Samples proportions here are pre-calculated in the form of precision and recall using micro-averaging
        # and accuracy.
        size = len(target)

        # precision, recall using micro averaging
        for metric in [metrics.precision_score, metrics.recall_score]:
            A = metric(target, data[s1], average='micro', labels=labels)
            B = metric(target, data[s2], average='micro', labels=labels)
            
            nA = size      # number of assigned yeses by system A
            nB = size      # number of assigned yeses by system B
            
            p = (A+B)/(2*size)
            z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='larger')
            tmpStr += repr(round(p_value, 4)).rjust(13)

            z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='smaller')
            tmpStr += repr(round(p_value, 4)).rjust(13)

            z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='two-sided')
            tmpStr += repr(round(p_value, 4)).rjust(13)
        
        # accuracy
        A = metrics.accuracy_score(target, data[s1])
        B = metrics.accuracy_score(target, data[s2])
        
        p = (A+B)/(2*size)
        z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='larger')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='smaller')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        z, p_value = sm.stats.proportions_ztest([A*size, B*size], [size, size], alternative='two-sided')
        tmpStr += repr(round(p_value, 4)).rjust(13)

        # ********* macro sign tests (S-test, binomial test)
        for metric in [metrics.f1_score, metrics.precision_score, metrics.recall_score]:
            # Generate results per category
            A = metric(target, data[s1], average=None, labels=labels)
            B = metric(target, data[s2], average=None, labels=labels)
            
            # n -> the number of times that A and B differ
            n = sum(i != j for i, j in zip(A, B))
            # k -> the number of times that A is larger than B
            k = sum(1 for i, j in zip(A, B) if i > j)
                    
            # 'n' is the number of trials in which the compared systems have different predictions
            # The hypothesized probability of success for system A to be greater than B is 50%
            p_value = stats.binom_test(k, n, alternative='greater')
            tmpStr += repr(round(p_value, 4)).rjust(13)

            p_value = stats.binom_test(k, n, alternative='less')
            tmpStr += repr(round(p_value, 4)).rjust(13)

            p_value = stats.binom_test(k, n, alternative='two-sided')
            tmpStr += repr(round(p_value, 4)).rjust(13)
            
            # ********* macro t-test (T-test, two-tailed paired t-test)
            t, p_value = stats.ttest_rel(A, B)
            tmpStr += repr(round(t, 4)).rjust(13)
            tmpStr += repr(round(p_value, 4)).rjust(13)
            
            # ********* macro t-test after rank transformation (T`-test, Wilcoxon)
            w, p_value = stats.wilcoxon(A, B)
            tmpStr += repr(round(w, 4)).rjust(13)
            tmpStr += repr(round(p_value, 4)).rjust(13)
            Wmean = np.mean(np.array(A) - np.array(B))
            tmpStr += repr(round(Wmean, 4)).rjust(13)
        
        logging.info(tmpStr)
    logging.info('')
    
    return

def generate_evaluation_graphs(results, outdir):
    """ Generate evaluation graphs
    
    Params:
    -------
    results :   list
        List  of named tuples
    outdir  :   string
        Output directory to save the generated graphs
    """
    # convert the list of namedtuples to a dataframe
    results_df = pd.DataFrame.from_records(results)
    results_df.columns = results[0]._fields

    # replace the names of the systems to be consistent with the table of results in the paper
    metric_map = { 'acc' : 'Accuracy',
                   'acc161' : 'Accuracy@161',
                   'mPrec' : 'Micro Precision',
                   'mRec' : 'Micro Recall',
                   'mF1' : 'Micro F1-score',
                   'MPrec' : 'Macro Precision',
                   'MRec' : 'Macro Recall',
                   'MF1' : 'Macro F1-score'}

    outdir = os.path.join(outdir, 'discrete')
    if(not os.path.isdir(outdir)):
        os.mkdir(outdir)
    
    for metric in metric_map:
        fig, ax = plt.subplots()
        markers = ['o', 'x', 's', 'd']
        linestyles = ['-', '--', '-.', ':']
        granularities = sorted(results_df['gran'].unique())
        
        for label, m, l in zip(granularities, markers, linestyles):
            data = results_df[['sys', metric]][results_df['gran'] == label]
            
            if label == granularities[0]:
                data.sort_values(metric, inplace=True, ascending=False)
                data.reset_index(inplace=True, drop=True)
                city_index_sorted = data.index
                city_sys_sorted = data['sys'].tolist()
                
            else:
                data.set_index('sys', inplace=True)
                data = data.loc[city_sys_sorted]
                data.reset_index(inplace=True)

            data.plot(x='sys', y=metric, ax=ax, label=label, legend=True, marker=m, linestyle=l, rot=90)
            ax.set_xticks(city_index_sorted)
            ax.set_xticklabels(data['sys'])
            ax.set_xlabel('Geolocation Systems')
            ax.set_ylabel(metric_map[metric])
        
        plt.title('Evaluation at the level of {}'.format(metric_map[metric]))
        plt.savefig(os.path.join(outdir, '{}.png'.format(metric_map[metric])), bbox_inches='tight')
        plt.show()
        plt.clf()
        plt.close()
    
    return

def measure_correlation(results, granularities, outdir):
    """ Calculate Kendall's Tau rank correlations
    
    Params:
    -------
    results         :   list
        List of Results named tuple
    granularities   :   list
        List of geographic granularities
    outdir          :   str
        Output directory to save the generated graphs
    
    Returns:
    --------
    correlations    :   dict of dict
        Dictionary of dictionary to save labels, tau-b, and p-values
        for all the correlated combinations
    """
    
    logging.info('')
    logging.info('*******************************************************')
    logging.info("Kendall's Tau-b rank correlations")

    correlations = defaultdict(dict)

    outdir = os.path.join(outdir, 'tau-b')
    if(not os.path.isdir(outdir)):
        os.mkdir(outdir)
    
    # ****  All metrics combinations at the same geographic granularity level
    measures = ['acc', 'acc161', 'medErr', 'meanErr', 'mPrec', 'mRec', 'mF1', 'MPrec', 'MRec', 'MF1']
    labels = ['Acc', 'Acc@161', 'Median', 'Mean', '$P_{\mu}$', '$R_{\mu}$', '$F_{\mu}$', '$P_{M}$', '$R_{M}$', '$F1_{M}$']
    
    logging.info('')
    logging.info('All metrics combinations at the same granularity level')
    
    for g in granularities:
        logging.info('')
        logging.info('At the level of {}'.format(g.upper()))
        
        corr = np.zeros((len(measures), len(measures)))
        p = np.zeros((len(measures), len(measures)))

        metrics_comb = list(combinations(measures, 2))
        for m1, m2 in metrics_comb:
            d1 = [getattr(r, m1) for r in results if r.gran == g]
            d2 = [getattr(r, m2) for r in results if r.gran == g]
            
            tau, tp = stats.kendalltau(d1, d2)
            corr[measures.index(m2), measures.index(m1)] = tau
            p[measures.index(m2), measures.index(m1)]    = tp
        
        correlations[g] = {'labels': metrics_comb, 'tau-b': corr, 'p-value': p}
        logging.info('labels:\n{}'.format(metrics_comb))
        logging.info('tau-b:\n{}'.format(corr))
        logging.info('p-value:\n{}'.format(p))
        logging.info('')
        
        with sns.axes_style("white"):
            mask = np.zeros_like(corr)
            mask[np.triu_indices_from(mask)] = True
            sns_hm = sns.heatmap(corr, mask=mask, square=True, annot=True, xticklabels=labels, yticklabels=labels, cmap="Oranges", cbar=False, linewidth=.5)
            plt.xticks(rotation=45)
            plt.title("Kendall's Tau-b Correlation Heatmap at the level of {}".format(g.capitalize()))
            sns_hm.figure.savefig(os.path.join(outdir, '{}_metric.png'.format(g)), bbox_inches='tight')
            plt.show()
            plt.clf()
    # **** End
    
    # ****  All granularity combinations for the same evaluation metric
    logging.info('')
    logging.info('*******************************************************')
    logging.info('All geographic granularity combinations for the same metric')
    
    granularity_comb = list(combinations(granularities, 2))
    discrete_metrics = [m for m in measures if m not in ['medErr, meanErr']]
    
    for g1, g2 in granularity_comb:
        logging.info('')
        logging.info('{}-{}'.format(g1.upper(), g2.upper()))
        
        corr = np.zeros((1, len(discrete_metrics)))
        p    = np.zeros((1, len(discrete_metrics)))

        for i, m in enumerate(discrete_metrics):
            d1 = [getattr(r, m) for r in results if r.gran == g1]
            d2 = [getattr(r, m) for r in results if r.gran == g2]
            
            tau, tp = stats.kendalltau(d1, d2)
            corr[0, i] = tau
            p[0, i]    = tp
        
        correlations['{}-{}'.format(g1, g2)] = {'labels': discrete_metrics, 'tau-b': corr, 'p-value': p}
        logging.info('labels:\n{}'.format(discrete_metrics))
        logging.info('tau-b:\n{}'.format(corr))
        logging.info('p-value:\n{}'.format(p))
        logging.info('')
        
        with sns.axes_style("white"):
            sns_hm = sns.heatmap(corr, square=True, annot=True, xticklabels=discrete_metrics, cmap="Oranges", cbar=False, linewidth=.5)
            plt.xticks(rotation=45)
            plt.title("Kendall's Tau-b Correlation Heatmap across {} and {}".format(g1.capitalize(), g2.capitalize()))
            sns_hm.figure.savefig(os.path.join(outdir, '{}_{}.png'.format(g1, g2)), bbox_inches='tight')
            plt.show()
            plt.clf()
    # **** End
    
    return correlations

def geoloceval(docs, graphsdir):
    """ Evaluate geolocation systems given their predictions    
    
    Params:
    -------
    docs        :   dict
        A dictionary of documents ids and their prediction based on systems including the ground-truth
    graphsdir   :   str
        Graphs directory
    """
    # split documents into a ground truth and a dictionary of systems with predictions in aligned lists
    systems = defaultdict(list)
    for d, v in docs.iteritems():
        for s, r in v.items():
            systems[s].append(r)
    target = systems.pop('oracle')

    error_dist = []
    labels = []
    for s, p in systems.iteritems():
        error_dist.append([float(r['error_dist']) for r in p])
        labels.append(s)

    # error distance evaluation
    evaluate_continuous(error_dist, labels, graphsdir)
    
    granularities = list(set(target[0].keys()).difference(set(['doc_id', 'lat', 'lon', 'error_dist'])))
    results = []
    Result = namedtuple('Results', ['sys', 'gran', 'acc', 'acc161', 'medErr', 'meanErr', 'mPrec', 'mRec', 'mF1', 'MPrec', 'MRec', 'MF1'])
    
    # discrete evaluation per granularity
    for g in granularities:
        logging.info('Discrete Evaluation at the level of {}'.format(g.upper()))
        
        # extract target labels on the level of g granularity
        l_target = [d[g] for d in target]
        lt_unresolved = len([p for p in l_target if p == 'unresolved'])
        logging.info('Number of originally unresolved locations: {} ({:.3%})'.format(lt_unresolved, float(lt_unresolved)/len(l_target)))
        data = defaultdict(list)
        
        for s in systems:
            # extract resolved labels at the level of g granularity
            l_pred = [d[g] for d in systems[s]]
            data[s] = l_pred
            
            # accuracy
            acc = metrics.accuracy_score(l_target, l_pred)

            # accuracy within 100 miles (161 km)
            l_pred161 = []
            error_dist = [float(d['error_dist']) for d in systems[s]]
            for e, t, p in zip(error_dist, l_target, l_pred):
                if e <= ERROR_DISTANCE_TOLERANCE:
                    l_pred161.append(t)
                else:
                    l_pred161.append(p)
            
            acc161 = metrics.accuracy_score(l_target, l_pred161)
            
            # median and mean error distances
            medErr = int(np.median(error_dist))
            meanErr = int(np.mean(error_dist))
            
            lp_unresolved = len([p for p in l_pred if p == 'unresolved'])
            logging.info('Number of unresolved locations for {}: {} ({:.3%})'.format(s, lp_unresolved, float(lp_unresolved)/len(l_pred)))
            
            # precision, recall and f1-score excluding unresolved locations from evaluation
            labels = list(set(l_target) - set(['unresolved']))
            micro = metrics.precision_recall_fscore_support(l_target, l_pred, labels=labels, average='micro')[:-1]
            macro = metrics.precision_recall_fscore_support(l_target, l_pred, labels=labels, average='macro')[:-1]

            results.append(Result._make((s, g, acc, acc161, medErr, meanErr,)+micro+macro))

        # measure frequentist statistical significance
        logging.info('')
        logging.info('Statistical significance test suite results')
        test_significance(l_target, data)
        
        # print results of g
        logging.info('')
        logging.info('Discrete evaluation results')
        tmpStr = str(' ').rjust(17)
        for m in Result._fields[2:]:
            tmpStr += str(m).rjust(10)
        logging.info(tmpStr)
        
        for s in [r for r in results if r.gran == g]:
            tmpStr = str(s.sys).ljust(17)
            for m in Result._fields[2:]:
                tmpStr += repr(round(getattr(s, m), 4)).rjust(10)
            logging.info(tmpStr)
        logging.info('')
    
    # generate evaluation graphs
    generate_evaluation_graphs(results, graphsdir)
    
    # measure tau-b rank correlations
    measure_correlation(results, granularities, graphsdir)

    return

def run(oracle, systems, outdir, api, geoloc, online):
    """ Main
    
    Params:
    -------
    oracle  :   str
        Ground-truth file path
    systems :   list
        List of input filepaths for the submitted systems
    outdir  :   str
        Output directory to save results in the generated format.
    """
    docs = defaultdict(dict)

    # read the cache of resolved gps coordinates if passed
    cached_coords = {}
    
    if geoloc:
        # check for existing file, load and test if all coords exist
        with codecs.open(geoloc, 'rb') as csvfile:
            reader = unicodecsv.reader(csvfile, encoding='utf-8', delimiter='\t', quoting=unicodecsv.QUOTE_MINIMAL)
            cached_coords.update([(literal_eval(row[0]), literal_eval(row[-1])) for row in reader])

    # load the ground-truth
    with codecs.open(oracle, 'r', 'utf-8') as foracle:
        logging.info('{}'.format(os.path.basename(oracle)))
        
        for line in foracle:
            data = json.loads(line)
            docs[data.pop('doc_id')]['oracle'] = data
        
        # resolve oracle coordinates to locations
        coords = [(d['oracle']['lat'], d['oracle']['lon']) for d in docs.values()]
        resolved_coords = resolve_to_location(coords, online, api, cached_coords)
        
        # update docs
        for k, v in izip(docs.keys(), resolved_coords):
            docs[k]['oracle'] = v
    
    for s in systems:
        with codecs.open(s, 'r', 'utf-8') as fsystem:
            logging.info('{}'.format(os.path.basename(s)))
            
            # extract system name
            sysname = os.path.basename(s).split('.')[0]
            for line in fsystem:
                data = json.loads(line)
                docs[data.pop('doc_id')][sysname] = data

            # resolve oracle coordinates to locations
            coords = [(d[sysname]['lat'], d[sysname]['lon']) for d in docs.values()]
            resolved_coords = resolve_to_location(coords, online, api, cached_coords)
            
            for k, v in izip(docs.keys(), resolved_coords):
                # calculate the error distance
                true_coords = (docs[k]['oracle']['lat'], docs[k]['oracle']['lon'])
                v['error_dist'] = great_circle(true_coords, (v['lat'], v['lon'])).km
                
                # update docs
                docs[k][sysname] = v

    # update/create cached coordinates
    if not geoloc and online:
        geoloc = os.path.join(outdir, '{}.coords_cache.txt'.format(api))
    
    if geoloc:
        with codecs.open(geoloc, 'w', 'utf-8') as outf:
            for k, v in cached_coords.iteritems():
                outf.write('{}\t{}\n'.format(k, v))
                    
            logging.info('Updated the offline coordinates cache.')
    
    # merge all input files into a single file
    outfile = os.path.join(outdir, 'geoloceval.output.json')
    with codecs.open(outfile, 'w', 'utf-8') as jfile:
        json.dump(docs, jfile, ensure_ascii=False, indent=4, separators=(',', ':'))
    logging.info('Saved formatted files to {}'.format(outfile))
    
    # create graphs directory
    graphsdir = os.path.join(outdir, 'graphs')
    if(not os.path.isdir(graphsdir)):
        os.mkdir(graphsdir)
    # evaluate
    geoloceval(docs, graphsdir)
    
    return

if __name__ == "__main__":
    def main(args):
        logfile = os.path.join(args.outdir, 'geoloceval.results.log')
        logging.basicConfig(filename=logfile, filemode='w', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level=logging.DEBUG)

        # systems' files
        systems = []
        for s in args.systems:
            if os.path.isfile(s):
                systems.append(s)
            elif os.path.isdir(s):
                systems += os.listdir(s)
            else:
                systems += glob.glob(s)
        
        run(args.oracle, systems, args.outdir, args.api, args.geoloc, args.online)

    parser = argparse.ArgumentParser(prog='GeoLocEval',
                                     description='Geographic Location Evaluation Framework for Geocoding Systems.')
    parser.add_argument('oracle', type=str, help='Ground-truth filepath')
    parser.add_argument('-d', '--outdir', type=str, default='.', help='Output directory')
    parser.add_argument('-g', '--geoloc', type=str, default=None, help='Filepath for cached geographic location mappings')
    parser.add_argument('-o', '--online', action='store_true', help='Use an online geocoding API; default=False (offline)')
    parser.add_argument('-a', '--api', choices=['nominatim', 'google'], default='nominatim', help='Geocoding API; default=nominatim')
    parser.add_argument('systems', nargs='+', help='File/List/Dir of systems\' input filepaths')
    parser.set_defaults(func=main)

    args = parser.parse_args()
    args.func(args)

